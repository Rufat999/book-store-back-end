package com.example.facebookbackend.entity;

import com.example.facebookbackend.model.BookType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Year;

@Entity
@Table(name = "books")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "author")
    private String author;
    @Column(name = "name")
    private String name;
    @Column(name = "bookType")
    private BookType bookType;
    @Column(name = "bookReleaseDate")
    private int bookReleaseDate;
}