package com.example.facebookbackend.exception;

public class RoleDoesNotExistException extends RuntimeException {
    private String message;

    public RoleDoesNotExistException(String message){
        super(message);
    }
}
