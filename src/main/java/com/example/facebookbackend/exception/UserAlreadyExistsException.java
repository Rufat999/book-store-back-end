package com.example.facebookbackend.exception;

public class UserAlreadyExistsException extends RuntimeException{
    private String message;
    public UserAlreadyExistsException(String message) {
        super(message);
    }
}