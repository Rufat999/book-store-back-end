package com.example.facebookbackend.exception;

public class PasswordMatchException extends RuntimeException{
    private String message;

    public PasswordMatchException(String message) {
        super(message);
    }
}
