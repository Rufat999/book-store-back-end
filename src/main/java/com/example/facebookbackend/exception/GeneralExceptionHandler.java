package com.example.facebookbackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
public class GeneralExceptionHandler {

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<?> userAlreadyExistException(UserAlreadyExistsException userAlreadyExistsException){
        userAlreadyExistsException.printStackTrace();
        return new ResponseEntity<>(userAlreadyExistsException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotNullException.class)
    public ResponseEntity<?> notNullException(NotNullException notNullException){
        notNullException.printStackTrace();
        return new ResponseEntity<>(notNullException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserNotExistException.class)
    public ResponseEntity<?> userNotExistException(UserNotExistException userNotExistException){
        userNotExistException.printStackTrace();
        return new ResponseEntity<>(userNotExistException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RoleDoesNotExistException.class)
    public ResponseEntity<?> roleDoesNotExistException(RoleDoesNotExistException roleDoesNotExistException){
        roleDoesNotExistException.printStackTrace();
        return new ResponseEntity<>(roleDoesNotExistException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BookReleaseDateException.class)
    public ResponseEntity<?> bookReleaseDateException(BookReleaseDateException bookReleaseDateException){
        bookReleaseDateException.printStackTrace();
        return new ResponseEntity<>(bookReleaseDateException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PasswordMatchException.class)
    public ResponseEntity<?> bookReleaseDateException(PasswordMatchException passwordMatchException){
        passwordMatchException.printStackTrace();
        return new ResponseEntity<>(passwordMatchException.getMessage(), HttpStatus.BAD_REQUEST);
    }
}