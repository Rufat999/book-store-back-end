package com.example.facebookbackend.exception;

public class BookReleaseDateException extends RuntimeException{
    private String message;

    public BookReleaseDateException(String message){
        super(message);
    }
}
