package com.example.facebookbackend.exception;

public class NotNullException extends RuntimeException{
    private String message;

    public NotNullException(String message) {
        super(message);
    }
}
