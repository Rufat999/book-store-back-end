package com.example.facebookbackend.dto;

import com.example.facebookbackend.model.RoleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    private Long id;
    private String surname;
    private String name;
    private String email;
    private String password;
    private RoleType roleType;
}
