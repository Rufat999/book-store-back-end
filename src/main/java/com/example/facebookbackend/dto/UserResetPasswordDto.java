package com.example.facebookbackend.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResetPasswordDto {

    private String email;
    private String password;
    private String confirmPassword;
}
