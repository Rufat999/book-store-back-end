package com.example.facebookbackend.service;

import com.example.facebookbackend.dto.*;
import com.example.facebookbackend.entity.User;
import com.example.facebookbackend.exception.PasswordMatchException;
import com.example.facebookbackend.exception.UserAlreadyExistsException;
import com.example.facebookbackend.exception.NotNullException;
import com.example.facebookbackend.exception.UserNotExistException;
import com.example.facebookbackend.model.RoleType;
import com.example.facebookbackend.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;

    public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper) {
        this.userRepository = userRepository;
        this.modelMapper = modelMapper;
    }


    @Override
    public List<UserResponse> getAllUsers() {
        List<User> user = userRepository.findAll();
        List<UserResponse> userResponse = user.stream().map(user1 -> modelMapper.map(user1, UserResponse.class)).collect(Collectors.toList());
        return userResponse;
    }

    @Override
    public UserResponse addUser(UserRequest request) {

//        List<User> users = userRepository.findAll();
//        for (User user : users) {
//            if (user.getEmail().equals(newUser.getEmail())) {
//                throw new EmailAlreadyExistsException("User already exists with " +
//                        "this email address" + newUser.getEmail());
//            }
//        }

        Optional<User> email = userRepository.findByEmail(request.getEmail());
        if (email.isPresent()) {
            throw new UserAlreadyExistsException("User already exists!");
        }
        String currentUserSurname = request.getSurname();
        String currentUserName = request.getName();
        String currentUserEmail = request.getEmail();
        String currentUserPassword = request.getPassword();

        if (Objects.equals(currentUserSurname, "") || Objects.equals(currentUserName, "") ||
                Objects.equals(currentUserEmail, "") || Objects.equals(currentUserPassword, "")) {
            throw new NotNullException("Enter something");
        }
//        List<User> users = userRepository.findAll();
//        for (User user : users) {
//            if (user.getEmail().equals(newUser.getEmail())) {
//                System.out.println("Email already exist");
//                return newUser;
//            }
//        }
//        userRepository.save(newUser);
//        System.out.println("User Posted!");
//        return newUser;
//        -----------------------------------------------------------2
//        userRepository.findByEmail(newUser.getEmail())
//                .orElseThrow(()->new EmailAlreadyExistsException("User is already exist" +
//                        " flowing by email: "+newUser.getEmail()));
//
//        boolean isEmailPresent = userRepository.findByEmail(newUser.getEmail()).isPresent();
//        if (userRepository.findByEmail(newUser.getEmail()).isPresent()){
//            userRepository.findByEmail(newUser.getEmail()).orElseThrow(
//                    () -> new EmailAlreadyExistsException("User is already exist flowing by email: "
//                            ));
//        }

        User user = modelMapper.map(request, User.class);
        userRepository.save(user);
        UserResponse userResponse = modelMapper.map(user, UserResponse.class);
        return userResponse;
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public Long getUsersCount() {
        return userRepository.count();
    }

    @Override
    public String login(LoginRequest request) {
        String currentUserEmail = request.getEmail();
        String currentUserPassword = request.getPassword();
        Optional<User> emailInDatabasePasswordInDatabase = userRepository.findByEmailAndPassword(request.getEmail(), request.getPassword());
        if (emailInDatabasePasswordInDatabase.isPresent()) {
            return "Successfully Complete";
        } else if (Objects.equals(currentUserEmail, "") || Objects.equals(currentUserPassword, "")) {
            throw new NotNullException("Enter something");
        } else {
            throw new UserNotExistException("Email And Password Not True");
        }
    }

    @Override
    public Optional<User> getUserById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public User updateUser(Long id, User newUser) {
        return userRepository.findById(id)
                .map(user -> {
                    user.setSurname(newUser.getSurname());
                    user.setName(newUser.getName());
                    user.setEmail(newUser.getEmail());
                    user.setPassword(newUser.getPassword());
                    return userRepository.save(user);
                })
                .orElseGet(() -> {
                    return userRepository.save(newUser);
                });
    }

    @Override
    public void userResetPassword(UserResetPasswordDto userResetPasswordDto) throws NotNullException {
        Optional<User> userExistsInDatabase = userRepository.findByEmail(userResetPasswordDto.getEmail());

        if (userExistsInDatabase.isPresent() && !Objects.equals(userResetPasswordDto.getPassword(), "") && userResetPasswordDto.getPassword().equals(userResetPasswordDto.getConfirmPassword())) {
            userExistsInDatabase.map(user -> {
                user.setSurname(user.getSurname());
                user.setName(user.getName());
                user.setEmail(user.getEmail());
                user.setPassword(userResetPasswordDto.getPassword());
                return userRepository.save(user);
            });
        } else if (userExistsInDatabase.isEmpty()) {
            throw new UserNotExistException("User Not Exists!");
        } else if (Objects.equals(userResetPasswordDto.getPassword(), "")) {
            throw new NotNullException("Enter something password!");
        } else if (!userResetPasswordDto.getPassword().equals(userResetPasswordDto.getConfirmPassword())) {
            throw new PasswordMatchException("Password Match Exception!");
        }
    }

    public void addUserRole(RoleType roleType, User user) {
        user.setRoleType(roleType);
        userRepository.save(user);
    }
}