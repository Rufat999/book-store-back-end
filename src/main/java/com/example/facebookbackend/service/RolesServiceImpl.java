//package com.example.facebookbackend.service;
//
//import com.example.facebookbackend.dto.RoleRequest;
//import com.example.facebookbackend.entity.Role;
//import com.example.facebookbackend.entity.User;
//import com.example.facebookbackend.repository.RolesRepository;
//import com.example.facebookbackend.repository.UserRepository;
//import org.modelmapper.ModelMapper;
//import org.springframework.stereotype.Service;
//
//import java.util.List;
//
//@Service
//public class RolesServiceImpl implements RolesService{
//
//    private final RolesRepository rolesRepository;
//    private final UserRepository userRepository;
//    private final ModelMapper modelMapper;
//
//
//    public RolesServiceImpl(RolesRepository rolesRepository, UserRepository userRepository, ModelMapper modelMapper) {
//        this.rolesRepository = rolesRepository;
//        this.userRepository = userRepository;
//        this.modelMapper = modelMapper;
//    }
//
//    @Override
//    public List<Role> getAllRoles() {
//        return rolesRepository.findAll();
//    }
//
//
//    @Override
//    public void addRole(Long userId, RoleRequest request) {
//        User user = userRepository.findById(userId)
//                        .orElseThrow(() -> new RuntimeException("User with id " + userId + " not found!"));
//        Role role = modelMapper.map(request, Role.class);
////        role.setUser(user);
////        user.getRoles().add(role);
//        userRepository.save(user);
//    }
//
//    @Override
//    public void deleteByName(String name) {
//        rolesRepository.deleteByName(name);
//    }
//
//    @Override
//    public void deleteAllRoles() {
//        rolesRepository.deleteAll();
//    }
//}