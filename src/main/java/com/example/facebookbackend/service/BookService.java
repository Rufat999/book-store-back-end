package com.example.facebookbackend.service;

import com.example.facebookbackend.entity.Book;
import com.example.facebookbackend.model.BookType;

import java.util.List;

public interface BookService {
    List<Book> getAllBooks();

    Book getBookById(Long id);

    Book getBookName(String name);

    void createBook(BookType bookType, Book book);

    List<Book> getBookByAuthor(String author);

    void updateBookName(Long id, Book book);

    void updateBookAuthor(Long id, Book book);

    void updateBook(Long id, Book book);

    void deleteByBookId(Long id);

    void deleteByBookName(String name);

    void deleteByBookAuthor(String author);

    List<Book> searchBooksByName(String query);

    List<Book> searchByBookAuthor(String query);

//    List<Book> searchByBookType(String query);
}
