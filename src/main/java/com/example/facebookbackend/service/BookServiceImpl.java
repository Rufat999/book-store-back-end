package com.example.facebookbackend.service;

import com.example.facebookbackend.entity.Book;
import com.example.facebookbackend.exception.BookReleaseDateException;
import com.example.facebookbackend.exception.NotNullException;
import com.example.facebookbackend.model.BookType;
import com.example.facebookbackend.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Book getBookById(Long id) {
        return bookRepository.findById(id).get();
    }

    @Override
    public Book getBookName(String name) {
        return bookRepository.findByName(name);
    }

    @Override
    public void createBook(BookType bookType, Book book) {
        book.setBookType(bookType);

        int thisYear = LocalDate.now().getYear();

        if (0 < book.getBookReleaseDate() && book.getBookReleaseDate() <= thisYear){
            bookRepository.save(book);
        }
        else {
            throw new BookReleaseDateException("Wrong year!");
        }
    }

    @Override
    public List<Book> getBookByAuthor(String author) {
        return bookRepository.findByAuthor(author);
    }

    @Override
    public void updateBookName(Long id, Book book) {
        Book updatedBook = bookRepository.findById(id).get();
        updatedBook.setName(book.getName());
        bookRepository.save(updatedBook);
        //"id": 1,
        //    "author": "Ruskin Bond",
        //    "name": "Listen to Your Heart: The London Adventure"
    }

    @Override
    public void updateBookAuthor(Long id, Book book) {
        Book updatedBook = bookRepository.findById(id).get();
        updatedBook.setAuthor(book.getAuthor());
        bookRepository.save(updatedBook);
    }

    @Override
    public void updateBook(Long id, Book book) {
        Book updatedBook = bookRepository.findById(id).get();

        int thisYear = LocalDate.now().getYear();

        if (0 < book.getBookReleaseDate() && book.getBookReleaseDate() <= thisYear){
            updatedBook.setName(book.getName());
            updatedBook.setAuthor(book.getAuthor());
            updatedBook.setBookReleaseDate(book.getBookReleaseDate());
            bookRepository.save(updatedBook);
        }
        else {
            throw new BookReleaseDateException("Wrong year!");
        }
    }

    @Override
    public void deleteByBookId(Long id) {
        bookRepository.deleteById(id);
    }

    @Override
    public void deleteByBookName(String name) {
        bookRepository.deleteByName(name);
    }

    @Override
    public void deleteByBookAuthor(String author) {
        bookRepository.deleteByAuthor(author);
    }

    @Override
    public List<Book> searchBooksByName(String query) {
        List<Book> books = bookRepository.searchBooks(query);
        return books;
    }

    @Override
    public List<Book> searchByBookAuthor(String query) {
        List<Book> books = bookRepository.searchByBookAuthor(query);
        return books;
    }

    public List<Book> searchAll(String query) {
        List<Book> books = bookRepository.searchAll(query);
        return books;
    }

//    @Override
//    public List<Book> searchByBookType(String query) {
//        List<Book> books = bookRepository.searchByBookType(query);
//        return books;
//    }


}