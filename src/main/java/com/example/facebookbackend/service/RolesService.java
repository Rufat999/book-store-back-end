//package com.example.facebookbackend.service;
//
//import com.example.facebookbackend.dto.RoleRequest;
//import com.example.facebookbackend.entity.Role;
//
//import java.util.List;
//
//public interface RolesService {
//    List<Role> getAllRoles();
//
//    void addRole(Long userId, RoleRequest request);
//
//    void deleteByName(String name);
//
//    void deleteAllRoles();
//}