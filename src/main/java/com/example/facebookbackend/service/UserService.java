package com.example.facebookbackend.service;

import com.example.facebookbackend.dto.LoginRequest;
import com.example.facebookbackend.dto.UserRequest;
import com.example.facebookbackend.dto.UserResetPasswordDto;
import com.example.facebookbackend.dto.UserResponse;
import com.example.facebookbackend.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    public List<UserResponse> getAllUsers();
    public UserResponse addUser(UserRequest request);
    public void deleteAll();

    void deleteUserById(Long id);

    Long getUsersCount();

    String login(LoginRequest request);

    Optional<User> getUserById(Long id);

    User updateUser(Long id, User user);

    void userResetPassword(UserResetPasswordDto userResetPasswordDto);
}