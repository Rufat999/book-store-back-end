package com.example.facebookbackend.repository;

import com.example.facebookbackend.entity.Book;
import com.example.facebookbackend.model.BookType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {
    Book findByName(String name);

    List<Book> findByAuthor(String author);

    void deleteByName(String name);

    void deleteByAuthor(String author);

    @Query("SELECT p FROM Book p WHERE " +
            "p.name LIKE CONCAT('%',:query, '%')")
    List<Book> searchBooks(String query);

    @Query("SELECT p FROM Book p WHERE " +
            "p.author LIKE CONCAT('%',:query, '%')")
    List<Book> searchByBookAuthor(String query);

//    @Query("SELECT p FROM Book p WHERE " +
//            "p.bookType LIKE CONCAT('%',:query, '%')")
//    List<Book> searchByBookType(String query);

    @Query("SELECT p FROM Book p WHERE p.name LIKE CONCAT('%',:query, '%')"
            + " OR p.author LIKE CONCAT('%',:query, '%')")
    List<Book> searchAll(String query);
}
