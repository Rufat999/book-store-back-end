//package com.example.facebookbackend.repository;
//
//import com.example.facebookbackend.entity.Role;
//import org.springframework.data.jpa.repository.JpaRepository;
//
//import java.util.Optional;
//
//public interface RolesRepository extends JpaRepository<Role, Long> {
//    void deleteByName(String name);
//
//    Optional<Role> findByName(String name);
//}
