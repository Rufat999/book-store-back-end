package com.example.facebookbackend.model;

public enum RoleType {

    ROLE_TYPE("Role Types"),
    Admin("Admin"),
    User("User");

    private final String name;

    RoleType(String name) {
        this.name = name;
    }
}
