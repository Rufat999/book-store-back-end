package com.example.facebookbackend.controller;

import com.example.facebookbackend.entity.Book;
import com.example.facebookbackend.model.BookType;
import com.example.facebookbackend.service.BookServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping(path = "/api/book")
public class BookController {

    private final BookServiceImpl bookService;

    public BookController(BookServiceImpl bookService) {
        this.bookService = bookService;
    }

    @GetMapping(path = "/all")
    public List<Book> getAllBooks(){
        return bookService.getAllBooks();
    }

    @GetMapping(path = "/byId/{id}")
    public Book getBookById(@PathVariable Long id){
        return bookService.getBookById(id);
    }

    @GetMapping(path = "/byName/{name}")
    public Book getBookByName(@PathVariable("name") String name){
        return bookService.getBookName(name);
    }

    @GetMapping(path = "/byAuthor/{author}")
    public List<Book> getBookByAuthor(@PathVariable("author") String author){
        return bookService.getBookByAuthor(author);
    }

    @PostMapping(path = "/create")
    public void createBook(@RequestParam ("bookType")BookType bookType, @RequestBody Book book){
        bookService.createBook(bookType, book);
    }

    @PutMapping(path = "/update/bookName/{id}")
    public void updateBookName(@PathVariable("id") Long id, @RequestBody Book book){
        bookService.updateBookName(id, book);
    }

    @PutMapping(path = "/update/bookAuthor/{id}")
    public void updateBookAuthor(@PathVariable("id") Long id, @RequestBody Book book){
        bookService.updateBookAuthor(id, book);
    }

    @PutMapping(path = "/update/book/{id}")
    public void updateBook(@PathVariable("id") Long id, @RequestBody Book book){
        bookService.updateBook(id, book);
    }

    @Transactional
    @DeleteMapping(path = "/delete/byBookId/{id}")
    public void deleteByBookId(@PathVariable("id") Long id){
        bookService.deleteByBookId(id);
    }
    @Transactional
    @DeleteMapping(path = "/delete/byBookName/{name}")
    public void deleteByBookName(@PathVariable("name") String name){
        bookService.deleteByBookName(name);
    }
    @Transactional
    @DeleteMapping(path = "/delete/byBookAuthor/{author}")
    public void deleteByBookAuthor(@PathVariable("author") String author){
        bookService.deleteByBookAuthor(author);
    }

    @GetMapping("/search/byBookName")
    public ResponseEntity<List<Book>> searchBooksByName(@RequestParam("query") String query){
        return ResponseEntity.ok(bookService.searchBooksByName(query));
    }

    @GetMapping("/search/byBookAuthor")
    public ResponseEntity<List<Book>> searchByBookAuthor(@RequestParam("query") String query){
        return ResponseEntity.ok(bookService.searchByBookAuthor(query));
    }

//    @GetMapping("/search/byBookType")
//    public ResponseEntity<List<Book>> searchByBookType(@RequestParam("query") String query){
//        return ResponseEntity.ok(bookService.searchByBookType(query));
//    }

    @GetMapping("/search/all")
    public ResponseEntity<List<Book>> searchAll(@RequestParam("query") String query) {
        return ResponseEntity.ok(bookService.searchAll(query));
    }
}