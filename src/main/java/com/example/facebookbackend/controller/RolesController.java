//package com.example.facebookbackend.controller;
//
//import com.example.facebookbackend.dto.RoleRequest;
//import com.example.facebookbackend.entity.Role;
//import com.example.facebookbackend.service.RolesServiceImpl;
//import org.springframework.transaction.annotation.Transactional;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//public class RolesController {
//
//    private final RolesServiceImpl rolesService;
//
//    public RolesController(RolesServiceImpl rolesService) {
//        this.rolesService = rolesService;
//    }
//
//
//    @GetMapping(path = "/all/roles")
//    public List<Role> getAllRoles(){
//        return rolesService.getAllRoles();
//    }
//
//    @PostMapping(path = "/{userId}/add/role")
//    public void addRole(@PathVariable Long userId, @RequestBody RoleRequest request){
//        rolesService.addRole(userId, request);
//    }
//
//    @Transactional
//    @DeleteMapping("/delete/{name}")
//    public void deleteByName(@PathVariable("name") String name){
//        rolesService.deleteByName(name);
//    }
//
//    @DeleteMapping(path = "/delete/all")
//    public void deleteAllRoles(){
//        rolesService.deleteAllRoles();
//    }
//}