package com.example.facebookbackend.controller;

import com.example.facebookbackend.dto.*;
import com.example.facebookbackend.entity.User;
import com.example.facebookbackend.model.RoleType;
import com.example.facebookbackend.service.UserServiceImpl;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@CrossOrigin("http://localhost:3000")
@RequestMapping("/api/users")
public class UserController {

    private final UserServiceImpl userService;

    public UserController(UserServiceImpl userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/all")
    public List<UserResponse> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping(path = "/count")
    public Long getUsersCount() {
        return userService.getUsersCount();
    }

    @GetMapping(path = "/{id}")
    public Optional<User> getUserById(@PathVariable("id") Long id){
        return userService.getUserById(id);
    }

    @PostMapping(path = "/login")
        public void login(@RequestBody LoginRequest request){
        userService.login(request);
        }

    @PostMapping(path = "/register")
        public void addUser(@RequestBody UserRequest request) {
            userService.addUser(request);
    }

    @PutMapping("/edit/{id}")
    public ResponseEntity<User> updateUser(@PathVariable("id") Long id, @RequestBody User user){
        userService.updateUser(id, user);
        return null;
    }

    @PutMapping("/resetPassword")
    public void userResetPassword(@RequestBody UserResetPasswordDto userResetPasswordDto){
        userService.userResetPassword(userResetPasswordDto);
    }

    @DeleteMapping(path = "/delete/{id}")
    public void deleteUserById(@PathVariable("id") Long id) {
        userService.deleteUserById(id);
    }

    @DeleteMapping(path = "/delete/all")
    public void deleteAll() {
        userService.deleteAll();
    }

    @PostMapping(path = "/add/user/role")
    public void addUserRole(@RequestParam("roleType") RoleType roleType, @RequestBody User user){
        userService.addUserRole(roleType, user);
    }
}